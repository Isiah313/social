class TextPostsController < ApplicationController
	
    
	def new
		@text_post = TextPost.new
	end


    #The create method builds a new text post for the current user using the params from the form. 
    #If it is able to save this new post, it redirects the user Image from book to the newly created post. 
    #Otherwise, it renders the new text post form Image from book again with an error message.
	def create
       @text_post = current_user.text_posts.build(text_post_params)
     if 
       @text_post.save 
       redirect_to post_path(@text_post), 
                           notice: "Post created!"
     else 
       render :new, alert: "Error creating post."
     end
  end

  def edit
      @text_post = current_user.text_posts.find(params[:id])
  end

  def update
      @text_post = current_user.text_posts.find(params[:id])
      if @text_post.update(text_post_params)
        redirect_to post_path(@text_post), notice: "Post updated!"
      else
        render :edit, alert: "Error updating post."
      end
  end

    private 
     def text_post_params 
       params.require(:text_post).permit(:title, :body)
     end

end

  
