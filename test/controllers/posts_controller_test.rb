require 'test_helper'

class PostsControllerTest < ActionController::TestCase
 test "redirects anonymous users to login"  do
 	get :index
 	assert_redirected_to login_url
 end
 	
 	test "get index for authenticated users" do
 	  user1 =  users(:user1)

 	  get :index, {}, { user_id: user1.id }
 	  assert_response :success
 end

    #initializes new user from fixtures
    test "should create post" do
	  user = users(:user1)

	#sets up necessary parameters for a text post
	  params = {
	   text_post: {
	   title: "Test Title",
	   body: "Test Body"
	    }
	   }

    #issues a post request to create a parasm hash with user.id 
      post :create, params, { user_id: user.id }

    #the new text post was persisted to the database and that the request redirects to the new post's URL
      text_post = assigns(:text_post)

      assert text_post.persisted?
      assert_redirected_to post_url(text_post)
   end


end
